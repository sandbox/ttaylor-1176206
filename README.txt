The master branch of IP Address Field is intentionally empty. Please clone a
proper version branch like '7.x-1.x' or '6.x-1.x'. For instructions see the
project's Git tab at http://drupal.org/project/1176206/git-instructions.
